package itja321.com;

public class Student
{

    private String studentID,
                   firstName,
                   lastName,
                   contactNumber,
                   address;
    
    public Student(String p_studentID,
                   String p_firstName,
                   String p_lastName,
                   String p_contactNumber,
                   String p_address)
    {
        setStudentID(p_studentID);
        setFirstName(p_firstName);
        setLastName(p_lastName);
        setContactNumber(p_contactNumber);
        setAddress(p_address);
    }

    String getStudentID()                         { return studentID;                }
    void setStudentID(String p_studentID)         { studentID = p_studentID;         }

    String getFirstName()                         { return firstName;                }
    void setFirstName(String p_firstName)         { firstName = p_firstName;         }

    String getLastName()                          { return lastName;                 }
    void setLastName(String p_lastName)           { lastName = p_lastName;           }

    String getContactNumber()                     { return contactNumber;            }
    void setContactNumber(String p_contactNumber) { contactNumber = p_contactNumber; }

    String getAddress()                           { return address;                  }
    void setAddress(String p_address)             { address = p_address;             }

}
