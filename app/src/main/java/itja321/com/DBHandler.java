package itja321.com;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBHandler extends SQLiteOpenHelper
{

    private static final String DB_NAME = "PIHE";

    public DBHandler(@Nullable Context context) { super(context, DB_NAME, null, 1); }

    boolean addStudent(Student s)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues row = new ContentValues();
        row.put("studentID", s.getStudentID());
        row.put("firstName", s.getFirstName());
        row.put("lastName", s.getLastName());
        row.put("contactNumber", s.getContactNumber());
        row.put("address", s.getAddress());

        return db.insert("students", null, row) != -1;
    }

    Student search(String p_studentID)
    {
        Student searchedStudent = null;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor rows = db.rawQuery
        (
            "SELECT * FROM students WHERE studentID = ?",
            new String[] { p_studentID }
        );

        if (rows.moveToFirst())
        {
            searchedStudent = new Student(rows.getString(0), rows.getString(1), rows.getString(2), rows.getString(3), rows.getString(4));
        }

        return searchedStudent;
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL
        (
            "CREATE TABLE students " +
            "( " +
                "studentID VARCHAR(40) PRIMARY KEY NOT NULL, " +
                "firstName VARCHAR(40) NOT NULL, " +
                "lastName VARCHAR(40) NOT NULL, " +
                "contactNumber VARCHAR(40) NOT NULL, " +
                "address VARCHAR(40) NOT NULL " +
            ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS students");
        onCreate(db);
    }
}
