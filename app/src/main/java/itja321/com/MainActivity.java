package itja321.com;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{

    EditText txtStudentID,
             txtFirstName,
             txtLastName,
             txtContactNumber,
             txtAddress;

    Button btnAdd,
           btnSave,
           btnSearch,
           btnClear;

    DBHandler dbh;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTitle("PIHE: STUDENT DETAILS");
        setContentView(R.layout.activity_main);

        txtStudentID     = findViewById(R.id.txtStudentID);
        txtFirstName     = findViewById(R.id.txtFirstName);
        txtLastName      = findViewById(R.id.txtLastName);
        txtContactNumber = findViewById(R.id.txtContactNumber);
        txtAddress       = findViewById(R.id.txtAddress);

        btnAdd    = findViewById(R.id.btnAdd);
        btnSave   = findViewById(R.id.btnSave);
        btnSearch = findViewById(R.id.btnSearch);
        btnClear  = findViewById(R.id.btnClear);

        dbh = new DBHandler(this);
    }

    public void add(View view)
    {
        btnAdd.setEnabled(false);
        btnSearch.setEnabled(false);

        txtFirstName.setEnabled(true);
        txtLastName.setEnabled(true);
        txtContactNumber.setEnabled(true);
        txtAddress.setEnabled(true);

        btnSave.setEnabled(true);
        btnClear.setEnabled(true);

        Toast.makeText(MainActivity.this, "Text Fields Enabled", Toast.LENGTH_LONG).show();
    }

    public void addToDatabase(View view)
    {
        String studentID     = txtStudentID.getText().toString(),
               firstName     = txtFirstName.getText().toString(),
               lastName      = txtLastName.getText().toString(),
               contactNumber = txtContactNumber.getText().toString(),
               address       = txtAddress.getText().toString();

        Student s = new Student(studentID, firstName, lastName, contactNumber, address);

        Toast.makeText(MainActivity.this, dbh.addStudent(s) ? "Student Inserted" : "Student Not Inserted", Toast.LENGTH_LONG).show();

        txtStudentID.setText("");
        txtFirstName.setText("");
        txtLastName.setText("");
        txtContactNumber.setText("");
        txtAddress.setText("");

        btnSave.setEnabled(false);
        btnClear.setEnabled(false);

        txtFirstName.setEnabled(false);
        txtLastName.setEnabled(false);
        txtContactNumber.setEnabled(false);
        txtAddress.setEnabled(false);

        btnAdd.setEnabled(true);
        btnSearch.setEnabled(true);
    }

    public void search(View view)
    {
        String studentID = txtStudentID.getText().toString();

        Student searchedStudent = dbh.search(studentID);

        btnClear.setEnabled(true);

        if (searchedStudent != null)
        {
            txtStudentID.setText(searchedStudent.getStudentID());
            txtFirstName.setText(searchedStudent.getFirstName());
            txtLastName.setText(searchedStudent.getLastName());
            txtContactNumber.setText(searchedStudent.getContactNumber());
            txtAddress.setText(searchedStudent.getAddress());
        }

        Toast.makeText(MainActivity.this, searchedStudent != null ? "Student Found" : "Student Not Found", Toast.LENGTH_LONG).show();
    }

    public void clear(View view)
    {
        txtStudentID.setText("");
        txtFirstName.setText("");
        txtLastName.setText("");
        txtContactNumber.setText("");
        txtAddress.setText("");

        btnClear.setEnabled(false);

        Toast.makeText(MainActivity.this, "Text Fields Cleared", Toast.LENGTH_LONG).show();
    }

}
